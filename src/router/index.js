import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login'),
  },
  {
    path: '/screen',
    name: 'screen',
    component: () => import('@/views/Screen'),
  },
  


  
// 测试使用
{
  path: '/scroll',  //自动滚屏显示
  name: 'scroll',
  component: () => import('@/views/Pagetest/ScrollPage'),
},
{
  path: '/polarity',
  name: 'polarity',
  component: () => import('@/views/Pagetest/PolarityPage'),
},
  {
    path: '/media',
    name: 'media',
    component: () => import('@/views/Pagetest/MediaPage'),
  },
  {
    path: '/HospitalNewsCount',
    name: 'HospitalNewsCount',
    component: () => import('@/views/Pagetest/HospitalNewsCountPage'),
  },
  {
    path: '/NewsNumberAnalyse',
    name: 'NewsNumberAnalyse',
    component: () => import('@/views/Pagetest/NewsNumberAnalysePage'),
  },
  {
    path: '/NewsKeywordRank',
    name: 'NewsKeywordRank',
    component: () => import('@/views/Pagetest/NewsKeywordRankPage'),
  },
  {
    path: '/NewsPolarity',
    name: 'NewsPolarity',
    component: () => import('@/views/Pagetest/NewsPolarityPage'),
  },
  {
    path: '/NewsYuntu',
    name: 'NewsYuntu',
    component: () => import('@/views/Pagetest/NewsYuntuPage'),
  },
]

const router = new VueRouter({
  routes
})

export default router