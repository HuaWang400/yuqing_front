import Vue from 'vue';
import { Button, Input,Form,FormItem,Row,Col,Select,Option,Table,TableColumn,TabPane,Tabs } from 'element-ui';
import VueRouter from 'vue-router';

Vue.use(Button)
Vue.use(Input)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Row)
Vue.use(Col)
Vue.use(Select)
Vue.use(Option)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(TabPane)
Vue.use(Tabs)
