import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    theme: 'chalk',
    hospital_id:'5',
    baseURL:"http://127.0.0.1:8000",
  },
  mutations: {
    changeTheme (state) {
      if (state.theme === 'chalk') {
        state.theme = 'vintage'
      } else {
        state.theme = 'chalk'
      }
    },
    changeHospital (state,hospital_id) {
      state.hospital_id=hospital_id
      }
    },

    actions: {
    },
    modules: {
    }
  
  
})
