import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
import store from './store'
import axios from 'axios';
import { getStore } from '@/utils/storage'

//引入全局样式文件  引入字体文件
import './assets/css/global.less';
import './assets/font/iconfont.css'
Vue.config.productionTip = false
Vue.prototype.$echarts =window.echarts
Vue.prototype.$http =axios 

// 设置公共的url
axios.defaults.baseURL =" http://127.0.0.1:8000/api/"
// 我们自己服务器的url
// axios.defaults.baseURL = 'http://49.235.88.178:3000';

//拦截请求 加入token
axios.interceptors.request.use(config => {
  const token = getStore('token');
  if (token) {
    // 表示用户已登录
    config.headers.common['Authorization'] =`jwt ${token}`;
  }
  return config
}, error => {
  return Promise.reject(error);
})


new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App)
}).$mount('#app')
