---

---

# yuqing_front

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



# 效果展示

base -url:http://localhost:8080/#

## 1.媒体类型

   /media --->Media.vue
   ![](README.assets/image-20210609133746912.png)


## 2.  医院展示
	/HospitalNewsCount-->HospitalNewsCount.vue
	
	![](README.assets/image-20210609131834789.png)


​	
​	
 ##3.所选医院舆情统计

   /NewsNumberAnalyse ---->NewsNumberAnalyse.vue

   ![](README.assets/image-20210609132019916.png)



## 4. 关键字南丁格尔图展示

   /NewsKeywordRank----->NewsKeywordRank.vue

   ![](README.assets/image-20210609132232975.png)

   

   

## 5 .词云图展示

   /NewsYuntu -->NewsYuntu.vue
   ![](README.assets/image-20210609132550351.png)

   

## 6.情感分析

   /NewsPolarity ---->NewsPolarity

   ![](README.assets/image-20210609132803783.png)



# 合并

​	换肤，实时时间，放大全屏等功能

​	/screen--->Screen.vue

![image-20210610120506982](README.assets/image-20210610120506982.png)



# 前端登录页面设计

使用jwt 认证登录

![image-20210610195313275](README.assets/image-20210610195313275.png)

# 基本效果图

![image-20210619233102846](README.assets/image-20210619233102846.png)